import { useState } from "react";
import { Container, Table, Row, Col, Form, Button } from "react-bootstrap";

// const newDatabaseMesinCuci = [
//   {
//     input: {
//       jumlahPakaian: 0,
//       tingkatKekotoran: 0,
//     },
//     fuzzifikasi: {
//       keanggotaanJumlahPakaian: {
//         sedikit: 0.0,
//         banyak: 0.0,
//       },
//       keanggotaanTingkatKekotoran: {
//         rendah: 0.0,
//         sedang: 0.0,
//         tinggi: 0.0,
//       },
//     },
//     inferensi: {
//       predikat: [
//         { lambat: 0.0, cepat: 0.0 },
//         { lambat: 0.0, cepat: 0.0 },
//         { lambat: 0.0, cepat: 0.0 },
//         { lambat: 0.0, cepat: 0.0 },
//         { lambat: 0.0, cepat: 0.0 },
//         { lambat: 0.0, cepat: 0.0 },
//       ],
//       predikatMax: {
//         lambat: 0.0,
//         cepat: 0.0,
//       },
//       titikPotong: [0.0, 0.0],
//     },
//     defuzzifikasi: {
//       momen: [0.0, 0.0, 0.0],
//       luas: [0.0, 0.0, 0.0],
//     },
//     output: 0,
//   },
// ];

const App = () => {
  const [mesinCuci, setMesinCuci] = useState([]);
  const [input, setInput] = useState({});

  const handleInputChange = (e) => {
    setInput({ ...input, [e.target.name]: parseInt(e.target.value) });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    let newMesinCuci = {};
    newMesinCuci.input = {
      jumlahPakaian: input.jumlahPakaian,
      tingkatKekotoran: input.tingkatKekotoran,
    };

    newMesinCuci.fuzzifikasi = fuzzifikasi(
      newMesinCuci.input.jumlahPakaian,
      newMesinCuci.input.tingkatKekotoran
    );

    newMesinCuci.inferensi = inferensi(
      newMesinCuci.fuzzifikasi.keanggotaanJumlahPakaian,
      newMesinCuci.fuzzifikasi.keanggotaanTingkatKekotoran
    );

    newMesinCuci.defuzifikasi = defuzifikasi(
      newMesinCuci.inferensi.predikatMax.lambat,
      newMesinCuci.inferensi.predikatMax.cepat,
      newMesinCuci.inferensi.titikPotong[0],
      newMesinCuci.inferensi.titikPotong[1]
    );

    newMesinCuci.output = output(
      newMesinCuci.defuzifikasi.momen,
      newMesinCuci.defuzifikasi.luas
    );
    // console.log(newMesinCuci);
    setMesinCuci((prevState) => [...prevState, newMesinCuci]);
  };

  // Membuat fungsi fuzzifikasi
  const fuzzifikasi = (pakaian, kekotoran) => {
    let result = {
      keanggotaanJumlahPakaian: {},
      keanggotaanTingkatKekotoran: {},
    };
    // Jumlah Pakaian
    if (pakaian <= 40) {
      result.keanggotaanJumlahPakaian.sedikit = 1;
      result.keanggotaanJumlahPakaian.banyak = 0;
    } else if (pakaian > 40 && pakaian < 80) {
      result.keanggotaanJumlahPakaian.sedikit = (80 - pakaian) / (80 - 40);
      result.keanggotaanJumlahPakaian.banyak = (pakaian - 40) / (80 - 40);
    } else if (pakaian >= 80) {
      result.keanggotaanJumlahPakaian.sedikit = 0;
      result.keanggotaanJumlahPakaian.banyak = 1;
    }
    // Tingkat Kekotoran
    if (kekotoran <= 40) {
      result.keanggotaanTingkatKekotoran.rendah = 1;
      result.keanggotaanTingkatKekotoran.sedang = 0;
      result.keanggotaanTingkatKekotoran.tinggi = 0;
    } else if (kekotoran > 40 && kekotoran < 50) {
      result.keanggotaanTingkatKekotoran.rendah = (50 - kekotoran) / (50 - 40);
      result.keanggotaanTingkatKekotoran.sedang = (kekotoran - 40) / (50 - 40);
      result.keanggotaanTingkatKekotoran.tinggi = 0;
    } else if (kekotoran === 50) {
      result.keanggotaanTingkatKekotoran.rendah = 0;
      result.keanggotaanTingkatKekotoran.sedang = 1;
      result.keanggotaanTingkatKekotoran.tinggi = 0;
    } else if (kekotoran > 50 && kekotoran < 60) {
      result.keanggotaanTingkatKekotoran.rendah = 0;
      result.keanggotaanTingkatKekotoran.sedang = (60 - kekotoran) / (60 - 50);
      result.keanggotaanTingkatKekotoran.tinggi = (kekotoran - 50) / (60 - 50);
    } else if (kekotoran >= 60) {
      result.keanggotaanTingkatKekotoran.rendah = 0;
      result.keanggotaanTingkatKekotoran.sedang = 0;
      result.keanggotaanTingkatKekotoran.tinggi = 1;
    }
    return result;
  };

  // Membuat fungsi inferensi
  const inferensi = (himpJumlahPakaian, himpTingkatKekotoran) => {
    let result = {
      predikat: [],
      predikatMax: {
        lambat: 0,
        cepat: 0,
      },
      titikPotong: [],
    };

    // Operasi Logika Menentukan Predikat
    let lambat, cepat;
    // Rule 1, Jika pakaian sedikit dan kekotoran rendah, maka putaran lambat
    lambat = Math.min(himpJumlahPakaian.sedikit, himpTingkatKekotoran.rendah);
    cepat = 0;
    result.predikat.push({ lambat: lambat, cepat: cepat });
    // Rule 2, Jika pakaian sedikit dan kekotoran sedang, maka putaran lambat
    lambat = Math.min(himpJumlahPakaian.sedikit, himpTingkatKekotoran.sedang);
    cepat = 0;
    result.predikat.push({ lambat: lambat, cepat: cepat });
    // Rule 3, Jika pakaian sedikit dan kekotoran tinggi, maka putaran cepat
    lambat = 0;
    cepat = Math.min(himpJumlahPakaian.sedikit, himpTingkatKekotoran.tinggi);
    result.predikat.push({ lambat: lambat, cepat: cepat });
    // Rule 4, Jika pakaian banyak dan kekotoran rendah, maka putaran lambat
    lambat = Math.min(himpJumlahPakaian.banyak, himpTingkatKekotoran.rendah);
    cepat = 0;
    result.predikat.push({ lambat: lambat, cepat: cepat });
    // Rule 5, Jika pakaian banyak dan kekotoran sedang, maka putaran cepat
    lambat = 0;
    cepat = Math.min(himpJumlahPakaian.banyak, himpTingkatKekotoran.sedang);
    result.predikat.push({ lambat: lambat, cepat: cepat });
    // Rule 6, Jika pakaian banyak dan kekotoran tinggi, maka putaran cepat
    lambat = 0;
    cepat = Math.min(himpJumlahPakaian.banyak, himpTingkatKekotoran.tinggi);
    result.predikat.push({ lambat: lambat, cepat: cepat });

    // Mengambil nilai max dari tiap himpunan predikat
    result.predikatMax.lambat = Math.max(
      ...result.predikat.map((el) => el.lambat)
    );
    result.predikatMax.cepat = Math.max(
      ...result.predikat.map((el) => el.cepat)
    );
    // console.log(result.predikat.map((el) => el.lambat));
    // console.log(...result.predikat.map((el) => el.lambat));

    // Mencari nilai titik potong
    /**
     * Mencari nilai titik potong bisa menggunakan subtitusi dari rumus mencari nilai keanggotaan
     * Ada 2 pilihan disini, menggunakan sisi miring milik himpunan lambat, atau sisi miring milik himpunan cepat
     * (karena posisi kedua titip potong berada di sisi miring).
     * Saya memilih menggunakan rumus sisi miring himpunan cepat
     * (titik_potong-500)/(1200-500) = nilai_keanggotaan
     * maka : titik_potong = (nilai_keanggotaan * (1200-500)) + 500
     */
    let tPot1 = result.predikatMax.lambat * (1200 - 500) + 500;
    let tPot2 = result.predikatMax.cepat * (1200 - 500) + 500;
    result.titikPotong.push(tPot1, tPot2);

    return result;
  };

  // Membuat fungsi defuzifikasi
  const defuzifikasi = (maxPredikatLambat, maxPredikatCepat, tPot1, tPot2) => {
    let result = {
      momen: [],
      luas: [],
    };

    // Menghitung Momen
    let m1, m2, m3;
    m1 =
      ((maxPredikatLambat * 1) / 2) * Math.pow(tPot1, 2) -
      ((maxPredikatLambat * 1) / 2) * Math.pow(0, 2);
    m2 =
      (Math.pow(tPot2, 2) * (28 * tPot2 - 21429)) / 60000 -
      (Math.pow(tPot1, 2) * (28 * tPot1 - 21429)) / 60000;
    m3 =
      ((maxPredikatCepat * 1) / 2) * Math.pow(1200, 2) -
      ((maxPredikatCepat * 1) / 2) * Math.pow(tPot2, 2);
    result.momen.push(m1, m2, m3);

    // Menghitung Luas
    let a1, a2, a3;
    a1 = maxPredikatLambat * tPot1 - maxPredikatLambat * 0;
    a2 =
      (tPot2 * (7 * tPot2 - 7143)) / 10000 -
      (tPot1 * (7 * tPot1 - 7143)) / 10000;
    a3 = maxPredikatCepat * 1200 - maxPredikatCepat * tPot2;
    result.luas.push(a1, a2, a3);

    return result;
  };

  // Membuat fungsi menghitung output
  const output = (arrMomen, arrLuas) => {
    arrMomen = arrMomen.reduce((total, el) => total + el, 0);
    arrLuas = arrLuas.reduce((total, el) => total + el, 0);

    return arrMomen / arrLuas;
  };

  console.log(mesinCuci);

  return (
    <>
      <Container fluid>
        <h1 className="text-center">Mesin Cuci dengan Kecepatan Otomatis</h1>
        <Table striped bordered hover>
          <thead>
            <tr className="text-center" style={{ verticalAlign: "middle" }}>
              <th rowSpan={2}>Percobaan ke-</th>
              <th colSpan={3}>Jumlah Pakaian</th>
              <th colSpan={4}>Tingkat Kekotoran</th>
              <th rowSpan={2}>Kecepatan Mesin Cuci</th>
            </tr>
            <tr className="text-center">
              <th>Input</th>
              <th>#Sedikit</th>
              <th>#Banyak</th>
              <th>Input</th>
              <th>#Rendah</th>
              <th>#Sedang</th>
              <th>#Tinggi</th>
            </tr>
          </thead>
          <tbody>
            {mesinCuci.map((data, i) => {
              return (
                <tr className="text-center" key={i}>
                  <td>{i + 1}</td>
                  <td>{data.input.jumlahPakaian} pcs</td>
                  <td>{data.fuzzifikasi.keanggotaanJumlahPakaian.sedikit}</td>
                  <td>{data.fuzzifikasi.keanggotaanJumlahPakaian.banyak}</td>
                  <td>{data.input.tingkatKekotoran}</td>
                  <td>{data.fuzzifikasi.keanggotaanTingkatKekotoran.rendah}</td>
                  <td>{data.fuzzifikasi.keanggotaanTingkatKekotoran.sedang}</td>
                  <td>{data.fuzzifikasi.keanggotaanTingkatKekotoran.tinggi}</td>
                  <td>{data.output.toFixed(3)} rpm</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        <small># = Nilai Keanggotaan Himpunan Fuzzy</small>
      </Container>
      <Container className="mt-5">
        <Row>
          <Col>
            <h3 className="text-center">Tambah Percobaan</h3>
            <hr className="w-75 mx-auto" />
          </Col>
        </Row>
        <Form onSubmit={handleFormSubmit}>
          <Row>
            <Col md={5}>
              <Form.Group className="mb-3" controlId="formJumlahPakaian">
                <Form.Label>Jumlah Pakaian :</Form.Label>
                <Form.Control
                  name="jumlahPakaian"
                  type="text"
                  placeholder="Masukkan jumlah pakaian"
                  onChange={handleInputChange}
                />
              </Form.Group>
            </Col>
            <Col md={5}>
              <Form.Group className="mb-3" controlId="formTingkatKekotoran">
                <Form.Label>Tingkat Kekotoran :</Form.Label>
                <Form.Control
                  name="tingkatKekotoran"
                  type="text"
                  placeholder="Masukkan tingkat kekotoran"
                  onChange={handleInputChange}
                />
              </Form.Group>
            </Col>
            <Col md={2}>
              <p className="d-md-block d-none mb-1 opacity-0">Tombol Tambah</p>
              <Button primary type="submit" className="w-100">
                Tambah
              </Button>
            </Col>
          </Row>
        </Form>
      </Container>
    </>
  );
};

export default App;
